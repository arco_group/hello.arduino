// -*- mode: c++; coding: utf-8 -*-

module Example {
    interface Light {
	void setState(bool state);
    };
};

